import Portfolio from '../core/portfolio'
import Storage from '../core/storage'
import assert from 'assert'
import debounce from 'lodash.debounce'
import util from './util'

const defaultPortfolioName = 'default'
const portfolioTransactionsKey = 'transactions'
const portfolioPrivateAssetsKey = 'private-assets'
const significantDigitsKey = 'significant-digits'

export default {
  created: function () {
    this.debouncedDerive = debounce(this.derive, 3000, { leading: true, trailing: true })
    this.debouncedDerive()
  },

  data: function () {
    return {
      exchangeRatesAsOf: {},
      newTransaction: {},
      portfolio: null,
      portfolioName: defaultPortfolioName,
      privateAssets: (function () {
        const fromStorage = Storage.get(defaultPortfolioName, portfolioPrivateAssetsKey)
        if (fromStorage !== null) {
          return fromStorage
        }
        return []
      }()),
      significantDigits: (function () {
        const fromStorage = Storage.get(defaultPortfolioName, significantDigitsKey)
        if (fromStorage !== null) {
          if (fromStorage === 'Infinity') {
            return Infinity
          }
          return fromStorage
        }
        return 3
      }()),
      summary: {},
      transactions: (function () {
        const fromStorage = Storage.get(defaultPortfolioName, portfolioTransactionsKey)
        if (fromStorage !== null) {
          return fromStorage
        }
        return [
          { date: '2017-01-01', baseAmount: -1000, baseCurrency: 'EUR', assetQuantity: 1.03, assetSymbol: 'BTC', note: 'Buy some Bitcoin', isDemo: true },
          { date: '2017-02-01', baseAmount: -1000, baseCurrency: 'EUR', assetQuantity: 95.2, assetSymbol: 'ETH', note: 'Buy some Ethereum' },
          { date: '2017-03-01', baseAmount: -1000, baseCurrency: 'EUR', assetQuantity: 0.86, assetSymbol: 'BTC', note: 'Buy more Bitcoin' },
          { date: '2017-05-22', baseAmount: 900, baseCurrency: 'EUR', assetQuantity: -0.5, assetSymbol: 'BTC', note: 'Sell some of the Bitcoins' },
          { date: '2017-12-12', baseAmount: -14600, baseCurrency: 'EUR', assetQuantity: 1, assetSymbol: 'BTC', note: 'FOMO' },
          { date: '2018-01-01', baseAmount: 11700, baseCurrency: 'EUR', assetQuantity: -1, assetSymbol: 'BTC', note: 'Panic sell' }
        ]
      }()),
      transactionsExtended: []
    }
  },

  methods: {
    addTransaction: function (e) {
      if (this.transactions.length > 0) {
        this.newTransaction.baseCurrency = this.transactions[0].baseCurrency
      }
      this.transactions.push(this.newTransaction)
      this.newTransaction = {}
    },

    deleteTransaction: function (index) {
      this.transactions.splice(index, 1)
    },

    importTransactions: function (newTransactionsStr) {
      const newTransactions = JSON.parse(newTransactionsStr)
      assert(Array.isArray(newTransactions))

      // Just assigning `this.transactions = newTransactions` does not trigger
      // Vue's reactivity.
      this.transactions.splice(0)
      this.transactions.push(...newTransactions)

      this.$router.push({ 'path': '/transactions' })
    },

    setExchangeRatesAsOf: function (timestamp) {
      const nowSecondsSinceEpoch = Math.round((new Date()).getTime() / 1000)
      const diff = nowSecondsSinceEpoch - timestamp

      if (this.exchangeRatesAsOf.timeoutId !== undefined) {
        window.clearTimeout(this.exchangeRatesAsOf.timeoutId)
      }

      this.exchangeRatesAsOf = {
        agoHumanReadable: util.timeAgoToHumanReadableString(diff),
        agoSeconds: diff,
        date: (new Date(timestamp * 1000)).toString()
      }

      let nextAfterSeconds
      if (diff < 60) {
        nextAfterSeconds = 10
      } else if (diff < 3600) {
        nextAfterSeconds = 60
      } else if (diff < 3600 * 24) {
        nextAfterSeconds = 3600
      } else {
        nextAfterSeconds = 3600 * 24
      }

      this.exchangeRatesAsOf.timeoutId = window.setTimeout(
        function () {
          this.setExchangeRatesAsOf(timestamp)
        }.bind(this),
        nextAfterSeconds * 1000
      )
    },

    shouldDisplayExchangeRatesAsOf: function () {
      return this.exchangeRatesAsOf.agoSeconds &&
        this.exchangeRatesAsOf.agoSeconds > 59 &&
        (this.$route.path === '/assets' ||
         this.$route.path === '/calculator' ||
         this.$route.path === '/transactions')
    },

    derive: async function () {
      if (this.transactions.length === 0) {
        this.transactionsExtended = []
        this.summary = {}
        return
      }

      this.$q.loading.show({
        delay: 0,
        message: 'Calculating...'
      })

      this.portfolio = new Portfolio(this.transactions, console.log)

      const privateRates = {}
      for (const p of this.privateAssets) {
        privateRates[p.symbol] = {}
        privateRates[p.symbol][this.transactions[0].baseCurrency] = Number(p.price)
        this.portfolio.exchange().addPrivateSymbol(p.symbol)
      }
      this.portfolio.exchange().addRates(privateRates)

      await this.redrawTables(false)

      this.setExchangeRatesAsOf(this.summary.timestamp)

      this.$q.loading.hide()
    },

    redrawTables: async function (onlyReformat) {
      this.transactionsExtended =
        await this.portfolio.transactionsExtended(onlyReformat, this.significantDigits)

      const summary = await this.portfolio.summary(onlyReformat, this.significantDigits)

      summary.footerRows = [{
        symbol: 'Total',
        flow: summary.total.flow,
        valuation: summary.total.valuation,
        net: summary.total.net,
        irr: summary.total.irr
      }]

      this.summary = summary
    },

    significantDigitsSet: function (d) {
      Storage.save(this.portfolioName, significantDigitsKey, d === Infinity ? 'Infinity' : d)
      this.significantDigits = d
      this.redrawTables(true) // returns Promise but we don't bother to wait for it with 'await'
    },

    tableSortGet: function (tableName) {
      // [{"colId":"date","sort":"desc"}]
      return Storage.get(this.portfolioName, tableName + '-table-sort')
    },

    tableSortSave: function (tableName, sortModel) {
      // sortModel=[{"colId":"date","sort":"desc"}]
      Storage.save(this.portfolioName, tableName + '-table-sort', sortModel)
    }
  },

  name: 'App',

  watch: {
    privateAssets: {
      deep: true,
      handler: function (newVal, oldVal) {
        Storage.save(this.portfolioName, portfolioPrivateAssetsKey, newVal)
        // this.debouncedDerive()
      }
    },
    transactions: {
      deep: true,
      handler: function (newVal, oldVal) {
        Storage.save(this.portfolioName, portfolioTransactionsKey, newVal)
        this.debouncedDerive()
      }
    }
  }
}
