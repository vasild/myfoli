
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', redirect: 'transactions' },
      { path: 'about', component: () => import('pages/about.vue') },
      { path: 'assets', component: () => import('pages/assets.vue') },
      { path: 'assets/private', component: () => import('pages/assets_private.vue') },
      { path: 'calculator', component: () => import('pages/calculator.vue') },
      { path: 'settings', component: () => import('pages/settings.vue') },
      { path: 'transactions', component: () => import('pages/transactions.vue') },
      { path: 'transactions/add', component: () => import('pages/transactions_add.vue') },
      { path: 'transactions/export', component: () => import('pages/transactions_export.vue') },
      { path: 'transactions/import', component: () => import('pages/transactions_import.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
