/*
Copyright 2019-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import assert from 'assert'
import { Entropy, charset64 } from 'entropy-string'

// import openpgp from 'openpgp'
const openpgp = require('openpgp')

/**
 * Generate a random password.
 * @return password
 */
function generatePassword () {
  const entropy = new Entropy({ charset: charset64 })
  const password = entropy.sessionID()

  assert(typeof password === 'string')
  assert(password.length > 10)

  return password
}

/**
 * Encrypt the given string with the given password.
 * @return encrypted string
 */
async function encrypt (
  /** [in] String to encrypt. */
  plaintext,
  /** [in] Encrypt with this password. */
  password) {
  const ciphertext = await openpgp.encrypt({
    armor: true,
    compression: openpgp.enums.compression.zlib,
    message: openpgp.message.fromText(plaintext),
    passwords: password
  })

  return ciphertext.data
}

/**
 * Decrypt a string encrypted using encrypt().
 * @return decrypted string
 */
async function decrypt (
  /** [in] Encrypted string to be decrypted, as returned by encrypt(). */
  encrypted,
  /** [in] Password to decrypt with, as supplied to encrypt(). */
  password) {
  const decrypted = await openpgp.decrypt({
    message: await openpgp.message.readArmored(encrypted),
    passwords: password
  })

  return decrypted.data
}

export default { generatePassword, encrypt, decrypt }
