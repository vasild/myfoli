/*
Copyright 2019-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Consider
// http://unhosted.org,
// http://sockethub.org,
// https://remotestorage.io,
// https://litewrite.net
// as an alternative of WebTorrent that would not require both devices to be online
// during the transfer from one to the other.
import WebTorrent from 'webtorrent'

export default class P2p {
  constructor (log, fatalErrorCb) {
    this.log = log
    this.fatalErrorCb = fatalErrorCb
    this.init()
  }

  init () {
    if (this.webTorrent) {
      return
    }

    this.webTorrent = new WebTorrent({
      tracker: {
        rtcConfig: {
          iceServers: [
            {
              credential: 'qK<l0W_s^a!)eug|g1d;R54AS',
              urls: [ 'turn:numb.viagenie.ca' ],
              username: 'regviagenie@myforest.net'
            },
            {
              credential: 'qK<l0W_s^a!)eug|g1d;R54AS',
              urls: [ 'stun:numb.viagenie.ca' ],
              username: 'regviagenie@myforest.net'
            }/*,
            { urls: 'stun:global.stun.twilio.com:3478?transport=udp' }
            */
          ]
        }
      }
    })

    this.webTorrent.on('error', err => {
      this.log.error(`WebTorrent client fatal error: ${err.message}`)
      this.webTorrent = null
      if (this.fatalErrorCb) {
        this.fatalErrorCb()
      }
    })
  }

  download (uri, successCb) {
    this.init()

    this.webTorrent.add(
      uri,
      torrent => {
        torrent.on(
          'done',
          () => {
            this.log.info('Downloaded')
            torrent.files[0].getBuffer(
              async (err, buf) => {
                if (err) {
                  this.log.error(
                    `Error retrieving the content of the first file in the torrent: ${err.message}`
                  )
                  return
                }
                successCb(buf.toString())
              }
            )
          }
        )

        torrent.on('error', err => {
          this.log.error(`Torrent error: ${err.message}`)
        })
      }
    )
  }

  remove (uri, successCb) {
    this.init()

    this.webTorrent.remove(uri, err => {
      if (err) {
        this.log.error(`Could not remove torrent: ${err.message}`)
      } else if (successCb) {
        successCb()
      }
    })
  }

  seed (content, name, successCb) {
    this.init()

    const buf = Buffer.from(content)
    buf.name = name

    this.webTorrent.seed(
      buf,
      { name: name },
      torrent => {
        this.log.info('Seeding')
        successCb(torrent.magnetURI)
      }
    )
  }
}
