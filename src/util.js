/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import assert from 'assert'

function timeAgoToHumanReadableString (diff) {
  const sIfMultiple = (n) => { return n === 1 ? '' : 's' }

  if (diff < 5) {
    return 'now'
  }
  if (diff < 60) {
    return `${diff} second${sIfMultiple(diff)} ago`
  }
  if (diff < 3600) {
    const mins = Math.round(diff / 60)
    return `${mins} minute${sIfMultiple(mins)} ago`
  }
  if (diff < 3600 * 24) {
    const hours = Math.round(diff / 3600)
    return `${hours} hour${sIfMultiple(hours)} ago`
  }
  const days = Math.round(diff / 3600 / 24)
  return `${days} day${sIfMultiple(days)} ago`
}

function generatePortfolioFileName (portfolioName) {
  const formatToNN = n => n < 10 ? ('0' + n) : n

  const now = new Date()
  const nowStr = '' +
    now.getFullYear() +
    formatToNN(now.getMonth() + 1) +
    formatToNN(now.getDate()) + '_' +
    formatToNN(now.getHours()) +
    formatToNN(now.getMinutes()) +
    formatToNN(now.getSeconds())

  return `portfolio-${portfolioName}-${nowStr}.json`
}

/**
 * Create a myfoli portfolio link from an URI and a password.
 * @return an obscure string representing both the URI and the password.
 */
function myfoliLinkCreate (uri, password) {
  const separator = ','
  assert(uri.indexOf(separator) === -1)
  assert(password.indexOf(separator) === -1)
  return password + ',' + uri
}

/**
 * Parse a myfoli portfolio link.
 * @return { uri: ..., password: ... }
 */
function myfoliLinkParse (link) {
  const a = link.split(',')
  return { password: a[0], uri: a[1] }
}

export default {
  timeAgoToHumanReadableString,
  generatePortfolioFileName,
  myfoliLinkCreate,
  myfoliLinkParse
}
