#!/bin/sh -m

# https://www.imagemagick.org/Usage/fonts/

#for font in `convert -list font |grep Font: |cut -b 9-` ; do
#  convert \
#    -size 1600x1600 \
#    xc:white \
#    -font "${font}" \
#    -pointsize 800 \
#    -draw 'fill "#5400ff" translate 900,1100 rotate 270 text 0,0 M' \
#    -draw 'fill "#ff5700" text 1000,1000 F' \
#    "logo-${font}.jpg"
#done

convert \
  -size 1470x1470 \
  xc:transparent \
  -font "Linux-Biolinum-G-Regular" \
  -pointsize 1200 \
  -draw 'fill "#5400ff" translate 805,1235 rotate 270 text 0,0 M' \
  -pointsize 1400 \
  -draw 'fill "#ff5700" text 800,1160 F' \
  mf_basic.png &

convert \
  -size 1470x1470 \
  xc:transparent \
  -font "Linux-Biolinum-G-Regular" \
  -pointsize 1180 \
  -draw 'fill "#ffffff" stroke "#ffffff" stroke-width 40 translate 805,1235 rotate 270 text 0,0 M' \
  -draw 'fill "#5400ff"                                  translate 805,1235 rotate 270 text 0,0 M' \
  -pointsize 1380 \
  -draw 'fill "#ffffff" stroke "#ffffff" stroke-width 40 text 790,1160 F' \
  -draw 'fill "#ff5700"                                  text 790,1160 F' \
  mf_outline.png &

convert \
  -size 1470x1470 \
  xc:transparent \
  -font "Linux-Biolinum-G-Regular" \
  -draw 'fill "#fcfcfc" circle 735,735 735,0' \
  -pointsize 972 \
  -draw 'fill "#5400ff" translate 790,1210 rotate 270 text 0,0 M' \
  -pointsize 1134 \
  -draw 'fill "#ff5700" text 785,1135 F' \
  mf_circle.png &

convert \
  -size 1470x1470 \
  xc:transparent \
  -font "Linux-Biolinum-G-Regular" \
  -draw 'fill "#fcfcfc" roundRectangle 0,0 1470,1470 350,350' \
  -pointsize 972 \
  -draw 'fill "#5400ff" translate 790,1210 rotate 270 text 0,0 M' \
  -pointsize 1134 \
  -draw 'fill "#ff5700" text 785,1135 F' \
  mf_round_square.png &

convert \
  -size 1470x1470 \
  xc:transparent \
  -font "Linux-Biolinum-G-Regular" \
  -draw 'fill "#fcfcfc" roundRectangle 0,200 1470,1320 350,350' \
  -pointsize 972 \
  -draw 'fill "#5400ff" translate 790,1210 rotate 270 text 0,0 M' \
  -pointsize 1134 \
  -draw 'fill "#ff5700" text 785,1135 F' \
  mf_round_rectangle.png &

convert -resize 64x64 mf_basic.png mf_basic_64x64.ico &
convert -resize 192x192 mf_outline.png mf_outline_192x192.png &
convert -resize 128x128 mf_basic.png ../src/statics/mf_basic_128x128.png &
convert -resize 152x152 mf_outline.png ../src/statics/icons/apple-icon-152x152.png &
convert -resize 16x16 mf_outline.png ../src/statics/icons/favicon-16x16.png &
convert -resize 32x32 mf_outline.png ../src/statics/icons/favicon-32x32.png &
convert -resize 128x128 mf_outline.png ../src/statics/icons/icon-128x128.png &
convert -resize 192x192 mf_outline.png ../src/statics/icons/icon-192x192.png &
convert -resize 256x256 mf_outline.png ../src/statics/icons/icon-256x256.png &
convert -resize 384x384 mf_outline.png ../src/statics/icons/icon-384x384.png &
convert -resize 512x512 mf_outline.png ../src/statics/icons/icon-512x512.png &
convert -resize 144x144 mf_outline.png ../src/statics/icons/ms-icon-144x144.png &

while fg >/dev/null 2>&1 ; do : ; done
