/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

'use strict'

import Exchange from './exchange-cryptocompare'
import assert from 'assert'
import formatNumber from './format-number'
import irr from './irr'
import { BigNumber, math } from './math'

class Portfolio {
  constructor (transactions, errorLog = undefined, progressLog = undefined) {
    this.assets_ = {}
    this.baseCurrency_ = undefined
    this.exchange_ = new Exchange()
    this.lastRecalcTimestamp_ = 0
    this.errorLog = errorLog === undefined ? function (msg) { } : errorLog
    this.progressLog = progressLog === undefined ? function (msg) { } : progressLog
    // Today, at 00:00:00 UTC
    this.today_ = new Date(new Date().setUTCHours(0, 0, 0, 0))
    this.total_ = {
      flow: BigNumber('0'),
      valuation: BigNumber('0'),
      irr: BigNumber('0')
    }
    this.transactionsExtended_ = []
    this.transactions_ = []

    try {
      this.fromArray(transactions)
    } catch (e) {
      try {
        this.fromJson(transactions)
      } catch (ee) {
        this.fromCsv(transactions)
      }
    }

    this.validateAndNormalizeTransactions()
  }

  fromArray (arr) {
    if (!Array.isArray(arr)) {
      throw new Error(`Cannot grasp portfolio input, not an array: ${arr}`)
    }

    /*
    obj.forEach(t => {
      // validate input
    })
    */

    this.transactions_ = arr
  }

  fromJson (jsonStr) {
    this.fromArray(JSON.parse(jsonStr))
  }

  fromCsv (csvStr) {
    const transactions = []

    for (const line of csvStr.split('\n')) {
      if (line.length === 0) {
        continue
      }

      const fields = line.split(',')

      transactions.push({
        date: fields[Portfolio.CSV_INDEX_DATE],
        baseAmount: fields[Portfolio.CSV_INDEX_BASE_AMOUNT],
        baseCurrency: fields[Portfolio.CSV_INDEX_BASE_CURRENCY],
        assetQuantity: fields[Portfolio.CSV_INDEX_ASSET_QUANTITY],
        assetSymbol: fields[Portfolio.CSV_INDEX_ASSET_SYMBOL],
        note: fields[Portfolio.CSV_INDEX_NOTE]
      })
    }

    this.fromArray(transactions)
  }

  async summary (onlyReformat, significantDigits) {
    if (!onlyReformat) {
      await this.recalcIfNecessary()
    }

    let summary = { assets: [], total: {}, timestamp: this.lastRecalcTimestamp_ }

    for (const assetSymbol of Object.keys(this.assets_)) {
      const asset = this.assets_[assetSymbol]

      let averageExchangePrice
      if (math.isZero(asset.quantity)) {
        averageExchangePrice = '?'
      } else {
        const p = math.multiply(math.divide(asset.flow, asset.quantity), -1)

        averageExchangePrice = formatNumber(
          p.toSignificantDigits(Portfolio.MAX_SIGNIFICANT_DIGITS),
          significantDigits
        )
      }

      summary.assets.push({
        symbol: assetSymbol,
        quantity: formatNumber(asset.quantity, significantDigits),
        currentMarketPrice: formatNumber(asset.price, significantDigits),
        averageExchangePrice: averageExchangePrice,
        flow: formatNumber(asset.flow, significantDigits),
        valuation: formatNumber(asset.valuation, significantDigits),
        net: formatNumber(asset.flow.plus(asset.valuation), significantDigits),
        irr: asset.irr === null ? '?' : formatNumber(asset.irr, significantDigits)
      })
    }

    summary.total.flow = formatNumber(this.total_.flow, significantDigits)
    summary.total.valuation = formatNumber(this.total_.valuation, significantDigits)
    summary.total.net = formatNumber(this.total_.flow.plus(this.total_.valuation),
      significantDigits)
    summary.total.irr = this.total_.irr === null ? '?' : formatNumber(this.total_.irr,
      significantDigits)

    return summary
  }

  async recalcIfNecessary () {
    const now = Math.round(new Date().getTime() / 1000)

    if (now < this.lastRecalcTimestamp_ + Portfolio.CACHE_SECONDS) {
      return
    }

    this.calcBaseCurrencyFromTransactions()

    await this.calcAssetsFromTransactions()

    await this.calcTransactionsExtended()

    await this.calcTotalFromAssets()

    this.lastRecalcTimestamp_ = now
  }

  calcBaseCurrencyFromTransactions () {
    for (const t of this.transactions_) {
      if (this.baseCurrency_ === undefined) {
        this.baseCurrency_ = t.baseCurrency
      } else if (this.baseCurrency_ !== t.baseCurrency) {
        throw new Error(
          `Not all transactions are in the same base currency ` +
          `(${this.baseCurrency_} != ${t.baseCurrency})`
        )
      }
    }
  }

  async calcAssetsFromTransactions () {
    for (const t of this.transactions_) {
      if (this.assets_[t.assetSymbol] === undefined) {
        this.assets_[t.assetSymbol] = {
          quantity: t.assetQuantity,
          flow: t.baseAmount
        }
      } else {
        const a = this.assets_[t.assetSymbol]
        a.quantity = a.quantity.plus(t.assetQuantity)
        a.flow = a.flow.plus(t.baseAmount)
      }
    }

    const assetSymbols = Object.keys(this.assets_)

    try {
      await this.progressLog('fetching exchange rates')
      await this.exchange_.fetchRates(assetSymbols, [ this.baseCurrency_ ])
    } catch (e) {
      await this.errorLog(
        `Could not prefetch exchange rates from ${assetSymbols} ` +
        `to ${this.baseCurrency_}: ${e.message}`
      )
    }

    for (const assetSymbol of assetSymbols) {
      const asset = this.assets_[assetSymbol]

      await this.progressLog(`calculating IRR for ${assetSymbol}`)

      try {
        asset.price = await this.exchange_.rate(assetSymbol, this.baseCurrency_)

        asset.valuation = asset.price.times(asset.quantity)

        asset.irr = await this.irr(assetSymbol)
      } catch (e) {
        /* If we cannot convert to base currency then we assume it is worthless. */
        await this.errorLog(
          `${assetSymbol} is worthless: Cannot convert ` +
          `${assetSymbol} to ${this.baseCurrency_}: ${e.message}`
        )

        asset.valuation = BigNumber('0')

        asset.price = BigNumber('0')

        asset.irr = BigNumber('-100')
      }
    }
  }

  async calcTotalFromAssets () {
    await this.progressLog('calculating total portfolio IRR')

    this.total_.flow = BigNumber('0')
    this.total_.valuation = BigNumber('0')

    for (const assetSymbol of Object.keys(this.assets_)) {
      const asset = this.assets_[assetSymbol]

      this.total_.flow = this.total_.flow.plus(asset.flow)
      this.total_.valuation = this.total_.valuation.plus(asset.valuation)
    }

    this.total_.irr = await this.irr()
  }

  async irr (onlyForAssetSymbol = undefined) {
    let cashFlows = []
    for (const t of this.transactions_) {
      if (onlyForAssetSymbol === undefined || onlyForAssetSymbol === t.assetSymbol) {
        cashFlows.push({ date: t.date, amount: t.baseAmount })
      }
    }

    // Assume we sell everything today.
    if (onlyForAssetSymbol === undefined) {
      for (const assetSymbol of Object.keys(this.assets_)) {
        cashFlows.push({ date: this.today_, amount: this.assets_[assetSymbol].valuation })
      }
    } else {
      cashFlows.push({ date: this.today_, amount: this.assets_[onlyForAssetSymbol].valuation })
    }

    try {
      return irr.percentage(cashFlows).toSignificantDigits(Portfolio.MAX_SIGNIFICANT_DIGITS)
    } catch (e) {
      await this.errorLog(
        `Could not calculate ` +
        (onlyForAssetSymbol === undefined ? `total IRR` : `IRR for ${onlyForAssetSymbol}`) +
        `: ${e.message}`)
      return null
    }
  }

  validateAndNormalizeTransactions () {
    this.transactions_ = this.transactions_.map(t => {
      assert(/[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(t.date))
      assert.notStrictEqual(Number(t.baseAmount), NaN)
      assert(/[0-9a-z]+/i.test(t.baseCurrency))
      assert.notStrictEqual(Number(t.assetQuantity), NaN)
      assert(/[0-9a-z]+/i.test(t.assetSymbol))

      return {
        date: new Date(t.date),
        dateStr: t.date,
        baseAmount: BigNumber(t.baseAmount),
        baseCurrency: t.baseCurrency.toUpperCase(),
        assetQuantity: BigNumber(t.assetQuantity),
        assetSymbol: t.assetSymbol.toUpperCase(),
        note: t.note
      }
    })
  }

  async calcTransactionsExtended () {
    await this.progressLog('calculating IRR for each transaction')

    this.transactionsExtended_ = []

    for (const t of this.transactions_) {
      const te = Object.assign({}, t)

      te.date = t.dateStr

      if (!math.isZero(t.assetQuantity)) {
        te.amountPerOne = math.abs(math.divide(t.baseAmount, t.assetQuantity))
      }

      try {
        te.valuation = await this.exchange_.convert(t.assetQuantity, t.assetSymbol, t.baseCurrency)

        te.net = te.valuation.plus(t.baseAmount)

        if (math.isZero(t.baseAmount)) {
          if (math.isPositive(te.valuation)) {
            te.irr = BigNumber(Infinity)
          } else if (math.isNegative(te.valuation)) {
            te.irr = BigNumber('-100')
          } else {
            te.irr = BigNumber('0')
          }
        } else if (math.isZero(te.valuation)) {
          te.irr = BigNumber('-100')
        } else {
          te.irr = irr.percentage([
            { date: t.date, amount: t.baseAmount },
            { date: this.today_, amount: te.valuation }
          ]).toSignificantDigits(Portfolio.MAX_SIGNIFICANT_DIGITS)
        }
      } catch (e) {
        // Leave valuation, net and irr members undefined.
      }

      this.transactionsExtended_.push(te)
    }
  }

  transactionsExtendedFormat (significantDigits) {
    return this.transactionsExtended_.map((t, i) => {
      let irr
      if (t.irr) {
        irr = formatNumber(t.irr, significantDigits)
      } else {
        irr = '?'
      }
      return {
        indexInTransactions: i,
        date: t.date,
        baseAmount: formatNumber(t.baseAmount, Infinity),
        baseCurrency: t.baseCurrency,
        assetQuantity: formatNumber(t.assetQuantity, Infinity),
        assetSymbol: t.assetSymbol,
        amountPerOne: t.amountPerOne ? formatNumber(t.amountPerOne, significantDigits) : '?',
        note: t.note,
        valuation: t.valuation ? formatNumber(t.valuation, significantDigits) : '?',
        net: t.net ? formatNumber(t.net, significantDigits) : '?',
        irr: irr
      }
    })
  }

  async transactionsExtended (onlyReformat, significantDigits) {
    if (!onlyReformat) {
      await this.recalcIfNecessary()
    }

    return this.transactionsExtendedFormat(significantDigits)
  }

  exchange () {
    return this.exchange_
  }
}
Portfolio.CSV_INDEX_DATE = 0
Portfolio.CSV_INDEX_BASE_AMOUNT = 1
Portfolio.CSV_INDEX_BASE_CURRENCY = 2
Portfolio.CSV_INDEX_ASSET_QUANTITY = 3
Portfolio.CSV_INDEX_ASSET_SYMBOL = 4
Portfolio.CSV_INDEX_NOTE = 5
Portfolio.CACHE_SECONDS = 600
Portfolio.MAX_SIGNIFICANT_DIGITS = 10

export default Portfolio
