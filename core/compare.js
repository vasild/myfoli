/*
Copyright 2019-2021 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

'use strict'

import glyphs from './glyphs'
import { BigNumber, abs } from './math'

function toBigNumber (a) {
  if (a === glyphs.MinusInf) {
    return BigNumber(-Infinity)
  }
  if (a === glyphs.ApproxZero) {
    return BigNumber('0')
  }
  if (a === glyphs.PlusInf) {
    return BigNumber(Infinity)
  }
  if (!isNaN(parseFloat(a))) {
    const last = a.slice(-1)
    let mul = BigNumber('1')
    if (last === 'K') {
      a = a.slice(0, -1)
      mul = BigNumber('1000')
    } else if (last === 'M') {
      a = a.slice(0, -1)
      mul = BigNumber('1000000')
    } else if (last === 'B') {
      a = a.slice(0, -1)
      mul = BigNumber('1000000000')
    }
    return BigNumber(a).times(mul)
  }
  return BigNumber(-Infinity)
}

export default {
  formattedNumbers: function (x, y) {
    return toBigNumber(x).comparedTo(toBigNumber(y))
  },

  formattedNumbersAbs: function (x, y) {
    return abs(toBigNumber(x)).comparedTo(abs(toBigNumber(y)))
  },

  strings: function (x, y) {
    return x < y ? -1 : (x > y ? 1 : 0)
  },

  byProperty: function (a, b, property) {
    return this.strings(a[property], b[property])
  }

}
