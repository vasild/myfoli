/*
Copyright 2018-2021 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

'use strict'

import glyphs from './glyphs'
import { BigNumber } from './math'

/**
 * Replace insignificant digits in a number with zeros.
 * For example, keeping 3 significant digits:
 * 0.056789 -> 0.0568
 * 98765432 -> 98800000
 * 12345678 -> 12300000
 * @return altered number (string)
 */
function formatNumber (
  /** [in] Number to alter (bignumber). */
  x,
  /** [in] Number of significant digits to keep (number). If Infinity, then return x as string. */
  significantDigits) {
  if (!x.isFinite()) {
    return x.isPositive() ? glyphs.PlusInf : glyphs.MinusInf
  }

  if (significantDigits === Infinity) {
    return x.toFixed()
  }

  if (x.isZero()) {
    return '0'
  }

  if (x.abs().lessThan(BigNumber('1').dividedBy(BigNumber('10').pow(significantDigits)))) {
    return glyphs.ApproxZero
  }

  let suffix = ''
  if (significantDigits <= 4) {
    const thousand = BigNumber('1000')
    const million = BigNumber('1000000')
    const billion = BigNumber('1000000000')
    const a = x.abs()
    if (a.greaterThanOrEqualTo(thousand) && a.lessThan(million)) {
      x = x.dividedBy(thousand)
      suffix = 'K'
    } else if (a.greaterThanOrEqualTo(million) && a.lessThan(billion)) {
      x = x.dividedBy(million)
      suffix = 'M'
    } else if (x.greaterThanOrEqualTo(billion)) {
      x = x.dividedBy(billion)
      suffix = 'B'
    }
  }

  return x.toSignificantDigits(significantDigits).toFixed() + suffix
}

export default formatNumber
