/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

'use strict'

import assert from 'assert'
import request from 'request-promise-native'
import { BigNumber, math } from './math'

class Exchange {

  constructor() {

    this.rates_ = {}
    this.privateSymbols_ = {}
  }

  async fetchRates() {
    // Create a bunch of Promises.
    let requests = [
      // Later URLs override same exchange pairs from earlier URLs.
      'https://myfo.li/_data/latest/hitbtc.json',
      'https://myfo.li/_data/latest/bittrex.json',
      'https://myfo.li/_data/latest/binance.json',
      'https://myfo.li/_data/latest/bitstamp.json',
    ].map(url => request(url))

    // Wait for all of them to be resolved.
    const results = await Promise.all(requests)

    for (const result of results) {
      this.rates_ = { ...this.rates_, ...JSON.parse(result).symbols }
    }
  }

  addPrivateSymbol (symbol) {
    this.privateSymbols_[symbol] = true
  }

  addRates(rates) {
    this.rates_ = { ...this.rates_, ...rates }
    for (const f of Object.keys(rates)) {
      for (const t of Object.keys(rates[f])) {
        assert.strictEqual(typeof (rates[f][t]), 'number')

        this.rates_[(f + t).toUpperCase()] = { last: rates[f][t] }
      }
    }
  }

  async rate(srcSymbol, dstSymbol) {

    if (srcSymbol === dstSymbol) {
      return BigNumber(1)
    }

    if (Object.keys(this.rates_).length == 0) {
      await this.fetchRates()
    }

    const forward = (srcSymbol + dstSymbol).toUpperCase()

    if (this.rates_[forward]) {
      return BigNumber(this.rates_[forward].last)
    }

    const reverse = (dstSymbol + srcSymbol).toUpperCase()

    if (this.rates_[reverse]) {
      return math.eval('1 / x', { x: BigNumber(this.rates_[reverse].last) } )
    }

    throw new RangeError(
      `No direct exchange rate between ${srcSymbol.toUpperCase()} and ${dstSymbol.toUpperCase()}`
    )
  }

  async convert(amount, srcSymbol, dstSymbol) {

    if (!amount || !srcSymbol || !dstSymbol) {
      throw new Error(
        `convert(amount=${amount}, srcSymbol=${srcSymbol}, dstSymbol=${dstSymbol}): invalid input`
      )
    }

    let a
    if (typeof amount === 'string') {
      a = BigNumber(amount)
    } else {
      a = amount
    }

    try {

      const directRate = await this.rate(srcSymbol, dstSymbol)

      return a.times(directRate)
    } catch (e) {

      if (!(e instanceof RangeError)) {
        throw e
      }

      const srcSymbolToBtcRate = await this.rate(srcSymbol, 'btc')
      const btcToDstSymbolRate = await this.rate('btc', dstSymbol)

      return a.times(srcSymbolToBtcRate).times(btcToDstSymbolRate)
    }
  }
}

export default Exchange
