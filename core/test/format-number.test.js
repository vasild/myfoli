/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import assert from 'assert'
import formatNumber from '../format-number'
import glyphs from '../glyphs'
import { BigNumber } from '../math'

describe('format-number', function () {
  it('3 significant digits', function () {
    for (const x of [
      { input: '0.0000000', expected: '0' },
      { input: '0.0000008', expected: glyphs.ApproxZero },
      { input: '0.0000078', expected: glyphs.ApproxZero },
      { input: '0.0000678', expected: glyphs.ApproxZero },
      { input: '0.0005678', expected: glyphs.ApproxZero },
      { input: '0.0045678', expected: '0.00457' },
      { input: '0.0345678', expected: '0.0346' },
      { input: '0.2345678', expected: '0.235' },
      { input: '2', expected: '2' },
      { input: '23', expected: '23' },
      { input: '234', expected: '234' },
      { input: '2345', expected: '2350' },
      { input: '23456', expected: '23500' },
      { input: '234567', expected: '235000' },
      { input: '2345678', expected: '2350000' },
    ]) {
      assert.equal(formatNumber(BigNumber(x.input), 3), x.expected)
      const minus = (x.expected === '0' || x.expected === glyphs.ApproxZero) ? '' : '-'
      assert.equal(formatNumber(BigNumber(`-${x.input}`), 3), minus + x.expected)
    }
  })

  it('7 significant digits', function () {
    for (const x of [
      { input: '0.0000000', expected: '0' },
      { input: '0.0000008', expected: '0.0000008' },
      { input: '0.0000078', expected: '0.0000078' },
      { input: '0.0000678', expected: '0.0000678' },
      { input: '0.0005678', expected: '0.0005678' },
      { input: '0.0045678', expected: '0.0045678' },
      { input: '0.0345678', expected: '0.0345678' },
      { input: '0.2345678', expected: '0.2345678' },
      { input: '2', expected: '2' },
      { input: '23', expected: '23' },
      { input: '234', expected: '234' },
      { input: '2345', expected: '2345' },
      { input: '23456', expected: '23456' },
      { input: '234567', expected: '234567' },
      { input: '2345678', expected: '2345678' },
    ]) {
      assert.equal(formatNumber(BigNumber(x.input), 7), x.expected)
      const minus = (x.expected === '0' || x.expected === glyphs.ApproxZero) ? '' : '-'
      assert.equal(formatNumber(BigNumber(`-${x.input}`), 7), minus + x.expected)
    }
  })

  it('infinite precision', function () {
    for (const x of [
      { input: '0.1234567890123456789', expected: '0.1234567890123456789' },
      { input: '5.1234567890123456789', expected: '5.1234567890123456789' },
      { input: '1234567890', expected: '1234567890' },
    ]) {
      assert.equal(formatNumber(BigNumber(x.input), Infinity), x.expected)
      assert.equal(formatNumber(BigNumber(`-${x.input}`), Infinity), '-' + x.expected)
    }
  })

  it('format infinity', function () {
    for (const x of [
      { input: -Infinity, expected: glyphs.MinusInf },
      { input: Infinity, expected: glyphs.PlusInf },
    ]) {
      assert.equal(formatNumber(BigNumber(x.input), 5), x.expected)
    }
  })
})
