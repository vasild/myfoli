/*
Copyright 2019-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import assert from 'assert'
import compare from '../compare'
import glyphs from '../glyphs'

describe('compare', function () {
  it('strings', function () {
    assert.strictEqual(compare.strings('a', 'b'), -1)
    assert.strictEqual(compare.strings('*', '*'), 0)
    assert.strictEqual(compare.strings('zy', 'zx'), 1)
  })

  it('formattedNumbers', function () {
    assert.strictEqual(compare.formattedNumbers('1', '2'), -1)
    assert.strictEqual(compare.formattedNumbers('5', '5'), 0)
    assert.strictEqual(compare.formattedNumbers('111', '101'), 1)
    assert.strictEqual(compare.formattedNumbers('aaa', '2'), -1)
    assert.strictEqual(compare.formattedNumbers('aaa', glyphs.MinusInf), 0)
    assert.strictEqual(compare.formattedNumbers(glyphs.MinusInf, '-10'), -1)
    assert.strictEqual(compare.formattedNumbers(glyphs.MinusInf, '0'), -1)
    assert.strictEqual(compare.formattedNumbers(glyphs.MinusInf, '10'), -1)
    assert.strictEqual(compare.formattedNumbers(glyphs.ApproxZero, '-10'), 1)
    assert.strictEqual(compare.formattedNumbers(glyphs.ApproxZero, '0'), 0)
    assert.strictEqual(compare.formattedNumbers(glyphs.ApproxZero, '10'), -1)
    assert.strictEqual(compare.formattedNumbers(glyphs.PlusInf, '-10'), 1)
    assert.strictEqual(compare.formattedNumbers(glyphs.PlusInf, '0'), 1)
    assert.strictEqual(compare.formattedNumbers(glyphs.PlusInf, '10'), 1)
  })
})
