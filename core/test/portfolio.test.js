/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import Portfolio from '../portfolio'
import assert from 'assert'
import fs from 'fs'
import glyphs from '../glyphs'
import path from 'path'

async function testInput(arg) {

  let portfolio = new Portfolio(arg)

  const summary = await portfolio.summary(false, 4)

  const btcIndex = summary.assets.findIndex(a => a.symbol === 'BTC')
  assert.notEqual(btcIndex, -1)
  assert.strictEqual(summary.assets[btcIndex].quantity, '1.234')
  assert.strictEqual(summary.assets[btcIndex].flow, '-10000')
}

async function testFileInput(file) {

  const transactionsStr = fs.readFileSync(file, 'utf-8')

  await testInput(transactionsStr)
}

function fullPath(relativePath) {
  return path.dirname(__filename) + '/' + relativePath
}

describe('portfolio', function () {
  it('from CSV', async function () {
    await testFileInput(fullPath('fixtures/portfolio01.csv'))
  })

  it('from JSON', async function () {
    await testFileInput(fullPath('fixtures/portfolio01.json'))
  })

  it('from Array', async function () {
    await testInput(require(fullPath('fixtures/portfolio01.json')))
  })

  it('from non-Array', async function () {
    assert.rejects(
      async () => {
        await testInput('nonarray')
      }
    )
  })

  it('worthless asset', async function () {
    const transactionsStr = fs.readFileSync(fullPath('fixtures/portfolio02.json'), 'utf-8')

    let portfolio = new Portfolio(transactionsStr)

    const summary = await portfolio.summary(false, 3)
  })

  it('repeat summary', async function () {
    const transactionsStr = fs.readFileSync(fullPath('fixtures/portfolio03.json'), 'utf-8')

    let portfolio = new Portfolio(transactionsStr, console.log, msg => {})

    const summary1 = await portfolio.summary(false, 3)
    const summary2 = await portfolio.summary(false, 3)
    const summary3 = await portfolio.summary(true, 3)

    assert.strictEqual(summary1.timestamp, summary2.timestamp)
    assert.strictEqual(summary1.timestamp, summary3.timestamp)

    assert.deepStrictEqual(summary1, summary2)
    assert.deepStrictEqual(summary1, summary3)
  })

  it('different base currency', async function () {
    const transactionsStr = fs.readFileSync(fullPath('fixtures/portfolio04.json'), 'utf-8')

    let portfolio = new Portfolio(transactionsStr)

    assert.rejects(
      async () => {
        await portfolio.summary(false, 3)
      },
      {
        message: /not all transactions are in the same base currency/i,
        name: 'Error'
      }
    )
  })

  it('positive flow for buying', async function () {
    const transactionsStr = fs.readFileSync(fullPath('fixtures/portfolio05.json'), 'utf-8')

    let portfolio = new Portfolio(transactionsStr)

    const summary = await portfolio.summary(false, 3)

    const btcIndex = summary.assets.findIndex(a => a.symbol === 'BTC')
    assert.notEqual(btcIndex, -1)

    assert.strictEqual(summary.assets[btcIndex].irr, '?')

    assert.notEqual(summary.total, undefined)
    assert.strictEqual(summary.total.irr, '?')
  })

  it('transactions extended', async function () {
    const transactionsStr = fs.readFileSync(fullPath('fixtures/portfolio06.json'), 'utf-8')

    let portfolio = new Portfolio(transactionsStr)

    await portfolio.transactionsExtended(false, 3)
    const transactionsExtended = await portfolio.transactionsExtended(true, 3)

    assert.strictEqual(transactionsExtended.length, 6)
    assert.strictEqual(transactionsExtended[1].irr, glyphs.PlusInf)
    assert.strictEqual(transactionsExtended[2].irr, '-100')
    assert.strictEqual(transactionsExtended[3].irr, '0')
    assert.strictEqual(transactionsExtended[4].irr, '-100')
    assert.strictEqual(transactionsExtended[5].irr, '?')
  })

  it('exchange', async function () {
    const transactionsStr = fs.readFileSync(fullPath('fixtures/portfolio06.json'), 'utf-8')

    let portfolio = new Portfolio(transactionsStr, console.log)

    assert.notEqual(portfolio.exchange(), undefined)
  })
})
