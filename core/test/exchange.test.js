/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import ExchangeCryptoCompare from '../exchange-cryptocompare'
import ExchangeMyFoli from '../exchange-myfoli'
import assert from 'assert'
import nock from 'nock'
import { BigNumber } from '../math'

function test(exchangeModuleName) {
  let exchange

  beforeEach(async function () {
    switch (exchangeModuleName) {
      case '../exchange-cryptocompare':
        exchange = new ExchangeCryptoCompare()
        break;
      case '../exchange-myfoli':
        exchange = new ExchangeMyFoli()
        break;
    }
  })

  describe('fetch', async () => {
    it('empty', async () => {

      await exchange.fetchRates([], [])
    })

    it('big', async () => {

      let big = []
      for (let i = 0; i < 100; i++) {
        big.push('BTC')
      }

      await exchange.fetchRates(big, big)
    })
  })

  describe('convert', async () => {
    it('null', async () => {

      await assert.rejects(
        exchange.convert(BigNumber('0'), null, 'ETH'),
        {
          message: /(Erroneous response|invalid input)/
        }
      )
    })

    it('nonexistent', async () => {

      await assert.rejects(
        exchange.convert(BigNumber('0'), 'NONEXISTENT', 'EUR'),
        {
          message: /(Erroneous response|No direct exchange rate between)/
        }
      )
    })

    it('private asset', async () => {

      const sym = 'PRIVATESYMBOL'
      const base = 'EUR'
      const price = 123
      const quantity = BigNumber('2')

      const rates = {}
      rates[sym] = {}
      rates[sym][base] = price

      exchange.addRates(rates)
      exchange.addPrivateSymbol(sym)

      const result = await exchange.convert(quantity, sym, base)

      assert.strictEqual(result.toFixed(4), quantity.times(price).toFixed(4))
    })

    it('without prefetch (bignumber)', async () => {

      const start = BigNumber('100.1')
      const mid = await exchange.convert(start, 'USD', 'ETH')
      const end = await exchange.convert(mid, 'ETH', 'USD')

      assert.strictEqual(start.toFixed(4), end.toFixed(4))
    })

    it('without prefetch (string)', async () => {

      const start = '100.1'
      const mid = await exchange.convert(start, 'USD', 'ETH')
      const end = await exchange.convert(mid, 'ETH', 'USD')

      assert.strictEqual(start, end.toFixed(1))
    })

    it('pairs', async () => {

      const pairs = [
        { from: 'BTC', to: 'EUR' },
        { from: 'USD', to: 'ETH' },
        { from: 'ARK', to: 'EUR' },
        { from: 'QTUM', to: 'GBYTE' },
        { from: 'ZRX', to: 'USD' },
        { from: 'LTC', to: 'LTC' },
      ]

      await exchange.fetchRates(pairs.map(p => p.from), pairs.map(p => p.to))

      for (const pair of pairs) {

        const start = BigNumber('12.3456')
        const mid = await exchange.convert(start, pair.from, pair.to)
        const end = await exchange.convert(mid, pair.to, pair.from)

        assert.strictEqual(start.toFixed(4), end.toFixed(4))
      }
    })

    it('communication failure', async () => {

      if (!nock.isActive()) {
        nock.activate()
      }
      nock(/.*/).persist().get(/.*/).reply(404)

      await assert.rejects(
        exchange.convert(BigNumber('1'), 'BTC', 'EUR'),
        {
          message: /404/
        }
      )

      nock.restore()
    })
  })
}

describe('exchange-cryptocompare', function () { test('../exchange-cryptocompare') })
describe('exchange-myfoli', function () { test('../exchange-myfoli') })
