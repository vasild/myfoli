/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import assert from 'assert'
import irr from '../irr'
import math from 'mathjs'

/*
https://docs.google.com/spreadsheets/d/1hZd6hrck6fi9fmSkBQKZsvCjDYNRxMOI-Cj54CQanu8/edit#gid=0
*/

describe('irr', function () {
  it('findRoot() extreme', function () {
    assert.throws(
      () => {
        function f(x) {
          return x
        }
        const x1 = math.bignumber('-1')
        const y1 = f(x1)
        // Just some big number, so that findRoot() would not get too close.
        // It halves the interval length at each step, 256 times before giving up.
        const x2 = math.eval('2 ^ 260')
        const y2 = f(x2)
        const threshold = math.eval('1 / 1000000')

        irr.findRoot(f, x1, y1, x2, y2, threshold)
      },
      {
        name: 'RangeError',
        message: /values did not converge to 0 quickly/i
      }
    )
  })

  it('explicit positive guess1', function () {
    assert.strictEqual(
      irr.fractional(
        [
          { date: new Date('2000-01-02'), amount: -2 },
          { date: new Date('2001-02-03'), amount: 1 },
          { date: new Date('2002-03-04'), amount: 1 },
        ],
        math.bignumber('1') // daily IRR
      ).toFixed(1),
      '0.0'
    )
  })

  it('explicit correct guess1', function () {
    assert.strictEqual(
      irr.fractional(
        [
          { date: new Date('2000-01-02'), amount: -100 },
          { date: new Date('2001-02-03'), amount: 60 },
          { date: new Date('2002-03-04'), amount: 60 },
        ],
        math.bignumber('0.0003095936536944') // daily IRR
      ).toFixed(1),
      '0.1'
    )
  })

  it('explicit guess1 and guess2 with 2 cash flows', function () {
    assert.strictEqual(
      irr.fractional(
        [
          { date: new Date('2000-01-02'), amount: -1 },
          { date: new Date('2001-02-03'), amount: 10 },
        ],
        math.bignumber('-0.5'), // daily IRR
        math.bignumber('0.5') // daily IRR
      ).toFixed(4),
      '7.2620' // fractional() returns [0, 1] -> [0%, 100%], so it equals 726.20% annual IRR
    )
  })

  it('explicit guess1 and guess2 with 3 cash flows', function () {
    assert.strictEqual(
      irr.fractional(
        [
          { date: new Date('2000-01-02'), amount: -1 },
          { date: new Date('2001-02-03'), amount: 10 },
          { date: new Date('2002-03-04'), amount: 10 },
        ],
        math.bignumber('-0.5'), // daily IRR
        math.bignumber('0.00602844') // daily IRR
      ).toFixed(4),
      '7.9690' // 796.90%
    )
  })

  it('invalid input - empty', function () {
    assert.throws(
      () => {
        irr.percentage([])
      },
      {
        name: 'RangeError',
        message: /there is not one positive and one negative cash flow/i
      }
    )
  })

  it('invalid input - all negative', function () {
    assert.throws(
      () => {
        irr.percentage([
          { date: new Date('2020-04-19'), amount: -1 },
          { date: new Date('2020-07-15'), amount: -2 },
        ])
      },
      {
        name: 'RangeError',
        message: /there is not one positive and one negative cash flow/i
      }
    )
  })

  it('invalid input - all positive', function () {
    assert.throws(
      () => {
        irr.percentage([
          { date: new Date('2020-04-19'), amount: 1 },
          { date: new Date('2020-07-15'), amount: 2 },
        ])
      },
      {
        name: 'RangeError',
        message: /there is not one positive and one negative cash flow/i
      }
    )
  })

  it('speed with 2 cash flows', function () {
    for (let i = 1; i < 200; i++) {
      irr.fractional([
        { date: new Date('2018-01-01'), amount: -i },
        { date: new Date('2019-01-01'), amount: i * 1.1 },
      ])
    }
  })

  it('irr01', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2018-01-01'), amount: -100 },
        { date: new Date('2019-01-01'), amount: 110 },
      ]).toFixed(4),
      '10.0000'
    )
  })

  it('irr02', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2018-01-01'), amount: 100 },
        { date: new Date('2019-01-01'), amount: -110 },
      ]).toFixed(4),
      '10.0000'
    )
  })

  it('irr03', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2018-01-01'), amount: -100 },
        { date: new Date('2018-09-25'), amount: 110 },
      ]).toFixed(4),
      '13.9162'
    )
  })

  it('irr04', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2018-01-01'), amount: -100 },
        { date: new Date('2018-01-25'), amount: 110 },
      ]).toFixed(4),
      '326.1022'
    )
  })

  it('irr05', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2019-08-30'), amount: -100 },
        { date: new Date('2023-03-25'), amount: 110 },
      ]).toFixed(4),
      '2.7058'
    )
  })

  it('irr06', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2019-05-05'), amount: -100 },
        { date: new Date('2020-05-05'), amount: 5 },
      ]).toFixed(4),
      '-94.9589'
    )
  })

  it('irr07', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2000-01-01'), amount: -100 },
        { date: new Date('2364-10-04'), amount: 110 },
      ]).toFixed(4),
      '0.0261'
    )
  })

  it('irr08', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2020-04-19'), amount: -100 },
        { date: new Date('2020-07-15'), amount: -150 },
        { date: new Date('2021-01-17'), amount: 120 },
        { date: new Date('2021-03-21'), amount: -70 },
        { date: new Date('2022-10-22'), amount: 800 },
      ]).toFixed(4),
      '75.8353'
    )
  })

  it('irr09', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2020-04-19'), amount: -1 },
        { date: new Date('2020-07-15'), amount: -2 },
        { date: new Date('2021-01-17'), amount: -3 },
        { date: new Date('2021-03-21'), amount: -4 },
        { date: new Date('2022-10-22'), amount: 3 },
      ]).toFixed(4),
      '-48.0936'
    )
  })

  it('irr10', function () {
    assert.throws(
      () => {
        irr.percentage([
          { date: new Date('2020-04-19'), amount: -1 },
          { date: new Date('2020-07-15'), amount: -2 },
          { date: new Date('2021-01-17'), amount: -3 },
          { date: new Date('2021-03-21'), amount: 1 },
          { date: new Date('2022-10-22'), amount: -4 },
        ]).toFixed(4)
      },
      {
        name: 'RangeError',
        message: /All tries produce negative NPVs/i
      }
    )
  })

  it('irr11', function () {
    assert.throws(
      () => {
        irr.percentage([
          { date: new Date('2020-04-19'), amount: 1 },
          { date: new Date('2020-07-15'), amount: 2 },
          { date: new Date('2021-01-17'), amount: 3 },
          { date: new Date('2021-03-21'), amount: -1 },
          { date: new Date('2022-10-22'), amount: 4 },
        ]).toFixed(4)
      },
      {
        name: 'RangeError',
        message: /All tries produce positive NPVs/i
      }
    )
  })

  it('irr12', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('1980-05-14'), amount: -1234.56 },
        { date: new Date('1985-09-19'), amount: -23.45 },
        { date: new Date('1987-01-05'), amount: -543.21 },
        { date: new Date('1991-04-09'), amount: 1.99 },
        { date: new Date('1992-12-12'), amount: 987.65 },
        { date: new Date('1996-07-01'), amount: -0.01 },
        { date: new Date('1999-09-09'), amount: 9.99 },
        { date: new Date('2012-12-12'), amount: -10.11 },
        { date: new Date('2015-05-06'), amount: 200.98 },
        { date: new Date('2020-02-02'), amount: -1 },
        { date: new Date('2020-02-03'), amount: 1 },
        { date: new Date('2025-10-22'), amount: 876.76 },
      ]).toFixed(4),
      '0.5325'
    )
  })

  // Same as irr12, but entries are shuffled (not in chronological order).
  it('irr13', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('1999-09-09'), amount: 9.99 },
        { date: new Date('1987-01-05'), amount: -543.21 },
        { date: new Date('1991-04-09'), amount: 1.99 },
        { date: new Date('1992-12-12'), amount: 987.65 },
        { date: new Date('2012-12-12'), amount: -10.11 },
        { date: new Date('2025-10-22'), amount: 876.76 },
        { date: new Date('1985-09-19'), amount: -23.45 },
        { date: new Date('2015-05-06'), amount: 200.98 },
        { date: new Date('2020-02-02'), amount: -1 },
        { date: new Date('1980-05-14'), amount: -1234.56 },
        { date: new Date('1996-07-01'), amount: -0.01 },
        { date: new Date('2020-02-03'), amount: 1 },
      ]).toFixed(4),
      '0.5325'
    )
  })

  it('irr14', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2000-01-02'), amount: -1 },
        { date: new Date('2001-02-03'), amount: 1 },
      ]).toFixed(4),
      '0.0000'
    )
  })

  it('irr15', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2020-01-01'), amount: 1000 },
        { date: new Date('2020-01-01'), amount: -1000.1 },
      ]).toFixed(4),
      '3.7172'
    )
  })

  it('irr16', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2020-01-01'), amount: 1000 },
        { date: new Date('2020-01-02'), amount: -1000.1 },
      ]).toFixed(4),
      '3.7172'
    )
  })

  it('irr17', function () {
    assert.strictEqual(
      irr.percentage([
        { date: new Date('2020-01-02'), amount: 1000 },
        { date: new Date('2020-01-01'), amount: -1000.1 },
      ]).toFixed(4),
      '-3.5840'
    )
  })
})
