/*
Copyright 2018-2018 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

'use strict'

import assert from 'assert'
import request from 'request-promise-native'
import { BigNumber, math } from './math'

class Exchange {
  constructor () {
    /** A map like { 'BTCEUR': 1234, 'ETHUSD': 456 }. */
    this.rates_ = {}

    /** A list of symbols that are not publicly tradeable and their rates are manually
     * set with addRates(..., true) and should not be attempted to be fetched from the
     * internet. For example: { 'FOO': true, 'BAR': true }. */
    this.privateSymbols_ = {}
  }

  async fetchRates (srcSymbols, dstSymbols) {
    function joinInSubRangesWithMaxLength (input, maxLength) {
      let result

      for (let numRanges = 1; ; numRanges++) {
        result = []
        let tooLong = false

        for (let i = 0; i < numRanges; i++) {
          const len = Math.round(input.length / numRanges)

          if (len === 0) {
            continue
          }

          const beg = len * i
          const end = len * (i + 1)
          const range = input.slice(beg, end).join(',')

          if (range.length <= maxLength) {
            result.push(range)
          } else {
            tooLong = true
            break
          }
        }

        if (!tooLong) {
          break
        }
      }

      return result
    }

    const srcSymbolsNoPrivate = srcSymbols.filter(s => !this.privateSymbols_[s])
    const dstSymbolsNoPrivate = dstSymbols.filter(s => !this.privateSymbols_[s])

    const fsymsParams = joinInSubRangesWithMaxLength(srcSymbolsNoPrivate, Exchange.MAX_FSYMS_LENGTH)
    const tsymsParams = joinInSubRangesWithMaxLength(dstSymbolsNoPrivate, Exchange.MAX_TSYMS_LENGTH)

    let urls = []

    for (const f of fsymsParams) {
      for (const t of tsymsParams) {
        urls.push('https://min-api.cryptocompare.com/data/pricemulti?' +
          `fsyms=${f.toUpperCase()}&tsyms=${t.toUpperCase()}&extraParams=myfo.li`)
      }
    }

    // Create a bunch of Promises.
    const requests = urls.map(url => request(url))

    // Wait for all of them to be resolved.
    const results = await Promise.all(requests)

    for (const result of results) {
      const parsed = JSON.parse(result)

      if (parsed.Response === 'Error') {
        throw new Error(`Erroneous response from cryptocompare.com: ${parsed.Message}`)
      }

      this.addRates(parsed)
    }
  }

  addPrivateSymbol (symbol) {
    this.privateSymbols_[symbol] = true
  }

  addRates (rates) {
    for (const f of Object.keys(rates)) {
      for (const t of Object.keys(rates[f])) {
        assert.strictEqual(typeof (rates[f][t]), 'number')

        this.rates_[this._srcDstToPair(f, t)] = rates[f][t]
      }
    }
  }

  async convert (amount, srcSymbol, dstSymbol, forceFetch = false) {
    const rate = await this.rate(srcSymbol, dstSymbol, forceFetch)

    let a
    if (typeof amount === 'string') {
      a = BigNumber(amount)
    } else {
      a = amount
    }

    return a.times(rate)
  }

  async rate (srcSymbol, dstSymbol, forceFetch) {
    const forwardPair = this._srcDstToPair(srcSymbol, dstSymbol)
    const reversePair = this._srcDstToPair(dstSymbol, srcSymbol)

    if (forceFetch ||
      (this.rates_[forwardPair] === undefined && this.rates_[reversePair] === undefined)) {
      await this.fetchRates([ srcSymbol ], [ dstSymbol ])
    }

    assert(this.rates_[forwardPair] || this.rates_[reversePair])

    if (this.rates_[forwardPair]) {
      return BigNumber(this.rates_[forwardPair])
    }

    return math.divide(BigNumber('1'), BigNumber(this.rates_[reversePair]))
  }

  _srcDstToPair (src, dst) {
    return (src + dst).toUpperCase()
  }
}
Exchange.MAX_FSYMS_LENGTH = 300
Exchange.MAX_TSYMS_LENGTH = 100

export default Exchange
