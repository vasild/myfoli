/*
Copyright 2018-2019 Vasil Dimov

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// https://en.wikipedia.org/wiki/Internal_rate_of_return

'use strict'

import assert from 'assert'
import { BigNumber, math } from './math'

/** Number of digits to print after the fractional "." in debug messages. */
const PRINT_PRECISION = 8

/** Multiplier to convert milliseconds to days. */
const MSEC_TO_DAYS = math.eval('1 / 1000 / 3600 / 24')

/** Print a diagnostic message. */
function log (
  /** [in] Message to print. */
  msg) {

  // console.log(msg)
}

/** Calculate the distance in days (rounded) between two Date objects.
 * @return integer number of days (later - earlier), could be negative */
function daysBetween (
  /** [in] earlier date */
  d1,
  /** [in] later date */
  d2) {
  const periodLengthMsec = BigNumber(d2 - d1)
  return math.round(math.multiply(periodLengthMsec, MSEC_TO_DAYS))
}

/** Calculate net present value (NPV) of some cash flows.
 * The cash flows can be at irregular intervals (not necessary at 1 year interval)
 * with precision of one day.
 * @return net present value */
function npv (
  /** [in] An array of cash flows, like
   * [ { date: Date, amount: Number }, ... ]. */
  cashFlows,
  /** [in] Internal rate of return, or inflation rate, according to which
   * to calculate the NPV. A fractional BigNumber, for example:
   * 0.5 to designate 50% and 1.0 for 100%. */
  r) {
  let npv = BigNumber('0')
  let expr = '0'
  for (const cashFlow of cashFlows) {
    const periodLengthD = daysBetween(cashFlows[0].date, cashFlow.date)
    const add = math.eval('a / (1 + r) ^ n', {
      a: BigNumber(cashFlow.amount),
      r: r,
      n: periodLengthD
    })
    npv = math.add(npv, add)
    expr +=
      ` + ${cashFlow.amount} / (1 + ${r.toFixed(PRINT_PRECISION)}) ^ ` +
      `${periodLengthD.toFixed(PRINT_PRECISION)}`
  }
  log(`npv(${r.toFixed(PRINT_PRECISION)}): ${expr} = ${npv.toFixed(PRINT_PRECISION)}`)
  return npv
}

/** Check cash flows' sanity.
 * Throws an exception if not valid. */
function checkInput (
  /** [in] An array of cash flows, like
   * [ { date: Date, amount: Number }, ... ]. */
  cashFlows) {
  let onePositive = false
  let oneNegative = false

  for (let cashFlow of cashFlows) {
    if (cashFlow.amount > 0) {
      onePositive = true
    }

    if (cashFlow.amount < 0) {
      oneNegative = true
    }

    if (onePositive && oneNegative) {
      return
    }
  }

  throw new RangeError('There is not one positive and one negative cash flow')
}

/** Check if two numbers have different signs (one is strictly positive and
 * the other is strictly negative).
 * @return true if different */
function haveDifferentSigns (
  /** [in] First number. */
  a,
  /** [in] Second number. */
  b) {
  return (math.isNegative(a) && math.isPositive(b)) ||
         (math.isPositive(a) && math.isNegative(b))
}

/** Check if a given value is the range (-threshold, +threshold).
 * @return true if in the range */
function isWithinThreshold (
  /** [in] Value to check if in range. */
  value,
  /** [in] Threshold to define range size. */
  threshold) {
  return math.smaller(math.abs(value), threshold)
}

/** Find an approximation of one root of the equation f(x) = 0.
 * The function f() must be continuous and defined in the range
 * [xGuess1, xGuess2]. f(xGuess1) and f(xGuess2) must have different
 * signs. This function is bisecting the [xGuess1, xGuess2] interval at
 * each step. It stops when f(x) is within the range [-threshold, +threshold].
 * @return a number x, such that f(x) = 0 */
function findRoot (
  /** [in] Function whose root to find. */
  f,
  /** [in] First guess for x. */
  xGuess1,
  /** [in] f(xGuess1). */
  fXGuess1,
  /** [in] Second guess for x. */
  xGuess2,
  /** [in] f(xGuess2). */
  fXGuess2,
  /** [in] Threshold to define how close we want to get to the actual root. */
  threshold) {
  assert(haveDifferentSigns(fXGuess1, fXGuess2),
    'Invalid input to findRoot(): f() of two guesses do not have different signs: ' +
         `f(${xGuess1.toFixed(PRINT_PRECISION)})=${fXGuess1.toFixed(PRINT_PRECISION)} and ` +
         `f(${xGuess2.toFixed(PRINT_PRECISION)})=${fXGuess2.toFixed(PRINT_PRECISION)}`
  )

  let x = [xGuess1, xGuess2]
  let y = [fXGuess1, fXGuess2]

  let leftIndex
  let rightIndex
  if (math.smaller(x[0], x[1])) {
    leftIndex = 0
    rightIndex = 1
  } else {
    leftIndex = 1
    rightIndex = 0
  }

  for (let i = 2; i < 256; i++) {
    x[i] = math.eval('(a + b) / 2', { a: x[leftIndex], b: x[rightIndex] })
    y[i] = f(x[i])

    if (isWithinThreshold(y[i], threshold)) {
      return x[i]
    }

    if (haveDifferentSigns(y[leftIndex], y[i])) {
      rightIndex = i
    } else {
      assert.strictEqual(
        haveDifferentSigns(y[i], y[rightIndex]),
        true,
        'Cannot find a root of f(x): during bisect: all of ' +
          `${y[leftIndex].toFixed(PRINT_PRECISION)}, ` +
          `${y[i].toFixed(PRINT_PRECISION)}, ` +
          `${y[rightIndex].toFixed(PRINT_PRECISION)} have the same sign`
      )

      leftIndex = i
    }
  }

  throw new RangeError('Cannot find a root of f(x): values did not converge to 0 quickly')
}

/** Convert a daily IRR into an annual IRR.
 * A compound interest is assumed.
 * @return annual fractional IRR (0.1 for 10%, 1.0 for 100% yearly) */
function annualizeDailyIrr (
  /** [in] Fractional daily IRR (0.1 for 10%, 1.0 for 100% daily). */
  dailyIrr) {
  return math.eval('(1 + dailyIrr) ^ 365 - 1', { dailyIrr: dailyIrr })
}

/** Find a second guess for IRR, such that its NPV has a different sign
 * compared to the first guess. Throw an exception if a guess cannot be found.
 * @return IRR guess (0.1 for 10%, 1.0 for 100% daily) */
function findGuess2 (
  /** [in] An array of cash flows, like
   * [ { date: Date, amount: Number }, ... ]. */
  cashFlows,
  /** [in] First IRR guess. */
  guess1,
  /** [in] NPV calculated for guess1. */
  npvGuess1) {
  function isGuessOk (npvGuess) {
    return math.isZero(npvGuess) || haveDifferentSigns(npvGuess, npvGuess1)
  }

  /* Go left and right from guess1 searching for NPV such that it has a different
   * sign than guess1's NPV. We know that IRR cannot be less than -1.
   * - going left we start with guess1 and approach -1, halving the distance at
   *   each step. E.g. if guess1 = 0, then we would try -0.5, -0.75, -0.875, ...
   * - going right we start with guess1 (or some small positive number if guess1
   *   is not positive) and double the guess at each step. E.g. if guess1 = 0,
   *   then we would try 0.0000001, 0.0000002, 0.0000004, ...  */

  let guess2Left = guess1
  let guess2Right = math.isPositive(guess1) ? guess1 : BigNumber('0.0000001')

  for (let i = 0; i < 256; i++) {
    guess2Left = math.eval('(-1 + guess2Left) / 2', { guess2Left: guess2Left })
    const npvGuess2Left = npv(cashFlows, guess2Left)
    if (isGuessOk(npvGuess2Left)) {
      return [ guess2Left, npvGuess2Left ]
    }

    guess2Right = math.multiply(guess2Right, 2)
    const npvGuess2Right = npv(cashFlows, guess2Right)
    if (isGuessOk(npvGuess2Right)) {
      return [ guess2Right, npvGuess2Right ]
    }
  }

  throw new RangeError(
    'Cannot find two IRR guesses such that their corresponding NPVs to have different signs. ' +
    'All tries produce ' + (math.isNegative(npvGuess1) ? 'negative' : 'positive') + ' NPVs')
}

/** Calculate fractional (0.1 for 10%, 1.0 for 100%) annual IRR.
 * @return fractional annual IRR (BigNumber) */
function fractional (
  /** [in] An array of cash flows, like
   * [ { date: Date, amount: Number }, ... ]. */
  cashFlows,
  /** [in] First IRR guess, optional. A good guess could speed up the search. */
  guess1 = BigNumber('0'),
  /** [in] Second IRR guess, optional. A good guess could speed up the search. */
  guess2 = undefined) {
  checkInput(cashFlows)

  /* This one is easy and we do not need to do iterative searching. We can derive
   * the solution directly. */
  if (cashFlows.length === 2) {
    const cf0 = cashFlows[0]
    const cf1 = cashFlows[1]

    const d = math.eval('- a1 / a0', {
      a1: BigNumber(cf1.amount),
      a0: BigNumber(cf0.amount)
    })
    let periodLengthD = daysBetween(cf0.date, cf1.date)
    if (math.isZero(periodLengthD)) {
      periodLengthD = BigNumber(1)
    }

    const dailyIrr = math.nthRoot(d, periodLengthD).minus(1)

    return annualizeDailyIrr(dailyIrr)
  }

  const threshold = math.eval('1 / 1000000')

  const npvGuess1 = npv(cashFlows, guess1)

  if (isWithinThreshold(npvGuess1, threshold)) {
    return annualizeDailyIrr(guess1)
  }

  let npvGuess2
  if (guess2 === undefined) {
    [ guess2, npvGuess2 ] = findGuess2(cashFlows, guess1, npvGuess1)
  } else {
    npvGuess2 = npv(cashFlows, guess2)
  }

  if (isWithinThreshold(npvGuess2, threshold)) {
    return annualizeDailyIrr(guess2)
  }

  const dailyIrr = findRoot(
    (r) => { return npv(cashFlows, r) },
    guess1,
    npvGuess1,
    guess2,
    npvGuess2,
    threshold
  )

  return annualizeDailyIrr(dailyIrr)
}

/** Calculate percentage (10 for 10%, 100 for 100%) annual IRR.
 * @return percentage annual IRR (BigNumber) */
function percentage (
  /** [in] An array of cash flows, like
     * [ { date: Date, amount: Number }, ... ]. */
  cashFlows) {
  return fractional(cashFlows).times(100)
}

// XXX This is included in the output because webpack's sideEffects does
// not pick it up. How to remove it?
// function lfuxcrjsxgqgtepf() {
//  console.log('utqxucvygrievqgj')
// }
// exports.lfuxcrjsxgqgtepf = lfuxcrjsxgqgtepf

export default { findRoot, fractional, math, percentage }
